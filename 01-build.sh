rm *.out
rm *.aux
rm *.blg
rm *.bbl
rm *.log
rm *.abs
rm main.bundle-diff*
rm *.fls
rm *.fdb_latexmk

pdflatex main.bundle.tex
bibtex main.bundle
pdflatex main.bundle.tex
pdflatex main.bundle.tex

OPEN_FILE=$1
if [ -n "${OPEN_FILE}" ]; then
    open main.bundle.pdf
fi
