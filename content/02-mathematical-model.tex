\section{Mathematical model}
\label{sec:mathematical-model}

\subsection{Model equations}
The mathematical modeling of tsunami waves is a multi-scale problem that usually requires coupling several algorithms to properly cover all the physical phenomena observed from wave generation and propagation in the ocean, to wave shoaling and run-up \citep{leveque2011tsunami, wang2009user, international35iugg}. Here we focus on tsunami generation and propagation for far field wave forecast since it is the first step in the representation of tsunamis; the mathematical equations and algorithms are simpler and better known; and it provides relevant information for the understanding and communication of tsunami hazard. 

In deep ocean, tsunamis have a characteristic wave length $L \approx 100$ km, a characteristic amplitude $a \approx 1$ m and a characteristic water depth of $ h_0  \approx 4$ km, i.e.  $h_0/L \ll 1$ and $a/h_0 \ll 1$ \citep{dean1991water}. Under these assumptions a suitable approximation is given by the  linear shallow water equations in spherical coordinates \citep{liu1995numerical}:

\begin{equation}
    \begin{gathered}
         \frac{\partial \eta }{\partial t} + 
        \frac{1}{R cos(\theta)} \left( 
        \frac{\partial M}{\partial \lambda} +
        \frac{\partial }{\partial \theta}(N \cos \theta)\right) = 0 \\
                \frac{\partial M}{\partial t} + 
        \frac{gh}{R \cos \theta} \frac{\partial \eta}{\partial \lambda} = fN \\
        \frac{\partial N}{\partial t} + 
        \frac{gh}{R} \frac{\partial \eta}{\partial \theta} = -fM 
    \end{gathered}
    \label{eq:swe}
\end{equation}
        
\noindent where $t$ is the time coordinate and $\lambda, \theta$ are the longitude and latitude geographical coordinates respectively; $\eta, M, N$ the wave height and longitudinal and latitudinal momentum components; $R =6378$ km is the earth's radius; $ g = 9.81$ m/s the earth's gravitational acceleration;  $h(\lambda, \theta)$ is the bathymetry at location $(\lambda, \theta)$, where $h>0$ indicates underwater floor; and $f = 2\omega \sin (\theta)$ is the Coriolis factor with $\omega = 7.29\times 10^{-5} [rad/s]$, the earth's rotation frequency.

Similarly to \cite{christgau2014comparison}, \cite{wang2009user} and \cite{international35iugg}, equations \eqref{eq:swe} are discretized using finite differences with a second order in space and time leapfrog scheme as:

\begin{equation}
    \begin{gathered}
    \frac{\eta^{n+1/2}_{i,j} -\eta^{n-1/2}_{i,j}}{\Delta t} +
\frac{1}{R\cos \theta_j} \left( 
  \frac{M ^n _ {i+1/2,j} -M ^n_{i-1/2, j }}{\Delta \lambda}
 +\right.\\\left.\frac{N^n_{i,j+1/2}\cos \theta_{j+1/2} - N^n_{i,j-1/2}\cos\theta_{j-1/2}}{\Delta t} 
\right) = 0 \\
    \frac{M^{n+1}_{i+1/2,j}-M^n_{i+1/2,j}}{\Delta t } + \frac{gh_{i+1/2,j}}{R\cos \theta_j} \frac{\eta^{n+1/2}_{i+1,j}-\eta^{n+1/2}_{i,j}}{\Delta \lambda} = fN' \\
    \frac{ N ^{n+1}_{i,j+1/2}- N^{n}_{i,j+1/2} }{\Delta t} +
   \frac{gh_{i,j+1/2}}{R}\left(
   \frac{\eta^{n+1/2}_{i,j+1}-\eta^{n+1/2}_{i,j}  }{\Delta \theta}
   \right) = - fM' \\
   N' = \frac{1}{4}\left( 
N^n_{i+1,j+1/2}+
N^n_{i+1,j-1/2}+
N^n_{i,j+1/2}+
N^n_{i,j-1/2}\right) \\
 M' = \frac{1}{4}\left( 
M^n_{i+1/2,j+1}+
M^n_{i+1/2,j}+
M^n_{i-1/2,j+1}+
M^n_{i-1/2,j}\right)
    \end{gathered}
\end{equation}

\noindent where $\Delta \lambda, \Delta \theta$ is the grid size; $\Delta $ the time step; and $i,j,n$ indicate values of the respective variable at time $n\Delta t$ and location $(i\Delta \lambda, j\Delta \theta)$. Given a domain and its bathymetry $h$, the input required to integrate equations \eqref{eq:swe} is contained in the initial and boundary conditions. As mentioned by \cite{wang2009user}, numerical stability is ensured by choosing $\Delta t = CFL \times \Delta s_{min}/c_{max}$, with $CFL < 1$ the Courant-Friedrichs-Lewy number, $c_{max} = \max_{ij}\sqrt{gh_{ij}}$ and $\Delta s_{min}$ the minimum geodesic distance between two vertically or horizontally adjacent grid nodes.

\subsubsection{Initial conditions}
Even though arbitrary initial conditions can be provided through $h, M$ and $N$, there are formulas that characterize the generations of the tsunami depending on a set of parameters. Two of these are implemented in Nami: earthquakes and asteroids. For earthquake generation we use a Finite Fault model, assuming that the water is at rest and a perturbation is added around the epicenter according to the formulas of  \cite{okada1985surface}. These formulas are an explicit solution to a linear elasticity problem that assumes that the area affected by the earthquake is rectangular and depends on parameters such as its length, width, hypocenter, fault slip, and the 3D orientation of the fault plane. Complex earthquakes can then be represented superposing several of these rectangles with different parameters depending on the heterogeneity of the earthquake being modeled.

For asteroid generated tsunamis, \cite{ward2000asteroid} describe several formulas that try to approximate the deformation of the water due to the impact of the asteroid depending on how its energy is converted into water waves. In Nami one of such formulas is implemented, and assumes that the initial surface has radial symmetry and can be described by a paraboloid of positive curvature whose size depends on the size, mass, density and speed of the impactor, as well as the amount of energy that is transmitted to the water from the asteroid.


\subsubsection{Boundary conditions}
Three types of boundaries are considered, usually named periodic, open and closed. Periodic boundaries repeat the information of the opposite boundary and are used for the East - West borders whenever the domain covers a full circle of latitude. Open boundaries approximately allow waves to leave the domain without reflections and are used whenever the domain does not cover a latitudinal circle; this boundary condition considers the trajectories of the characteristics of the Riemann invariants of the wave equations to extrapolate values in time as explained by \cite{international35iugg}. Closed boundaries are used to simulate a reflecting wall by imposing $\eta = M = N = 0 $; this is used to model continents shorelines as a closed internal boundary, which makes sense at the large scale, where  run-up displacements are negligible \citep{international35iugg}.