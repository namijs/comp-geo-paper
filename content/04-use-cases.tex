\section{Use case examples}
\label{sec:use_cases}
\subsection{Example  1: Model verification and visualization of maximum amplitudes }

Here we consider the basic use case of Nami where the user, such as a scientist, needs to configure and run a simulation to post-process the hydrodynamic variables to generate figures such as maps of maximum amplitudes and line plots with time series. Examples of these are in figures \ref{fig:energy2010}, \ref{fig:timeseries2010}, \ref{fig:energy2011}, and \ref{fig:timeseries2011}, which correspond to the 8.8 Mw 2010 Maule earthquake and 9.1 Mw 2011 Tohoku earthquake.

To configure a simulation the user has to characterize the scenario, provide output options and optional life cycle callbacks to catch key events of the simulation. An example standalone HTML/Javascript code that that can be run from the web browser is shown in figure  \ref{fig:code-example}.


\begin{figure}
    \centering
    \input{figures/code-example.tex}
    \caption{Example standalone HTML file with Javascript code for configuring Nami to produce heat maps of figures \ref{fig:energy2010} and \ref{fig:energy2011}}
    \label{fig:code-example}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{figures/energy2010.png}
    \caption{``Heat maps'' of maximum wave amplitude and arrival time isochrones calculated by Nami for the 9.1 Mw scenario of Tohoku, Japan, 2011 and DART buoys locations.}
    \label{fig:energy2010}
\end{figure}


\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{figures/timeseries2010.png}
    \caption{Time series of wave amplitude calculated by Nami (blue line), EasyWave (red line) and measurements (black line) at DART buoys shown in figure \ref{fig:energy2010}}
    \label{fig:timeseries2010}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{figures/energy2011.png}
    \caption{``Heat maps'' of maximum wave amplitude and arrival time isochrones calculated by Nami for the 9.1 Mw scenario of Tohoku, Japan, 2011 and DART buoys locations.}
    \label{fig:energy2011}
\end{figure}


\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{figures/timeseries2011.png}
    \caption{Time series of wave heights calculated by Nami (blue line), EasyWave (red line) and measurements (black line) at DART buoys shown in figure \ref{fig:energy2011}}
    \label{fig:timeseries2011}
\end{figure}




For each scenario, bathymetry and earthquake information are provided through external files. The bathymetry source is ETOPO-1 \citep{amante2009etopo1} and the earthquake is represented by Finite Fault models proposed by \cite{delouis2010slip} and \cite{usgs2011earthquake}  for the scenarios of Chile and Japan respectively, using the formulas mentioned in section \ref{sec:mathematical-model}. As shown in figure \ref{fig:code-example} the domain covers the spherical rectangle $[90,325.83]\times[-60,70]$ and is discretized in a spherical uniform grid of $4717\times 2600 \approx 12.26$ million nodes with a spacing of 3 minutes. Each simulation is run until 25 hours of propagation (the \verb;stopTime; parameter) with a timestep of 2.9365 seconds, configured by default for a CFL number of 0.5; all boundary conditions are open.

Since the goal in this use case is to extract the computed hydrodynamic variables and plot them from another environment such as a Jupyter Notebook \citep{kluyver2016jupyter}, the only provided lifecycle callback is
 the \verb;simulationDidFinish; function (see figure \ref{fig:architecture-lifecycle}), where it commands the Nami \verb;Controller; to export gridded values of maximum amplitudes and arrival times to files after the simulation has reached the \verb;stopTime;.
 
Simulations were calculated on a computer with an Intel Core i7-7700HQ 2.8GHz CPU and 8GB of RAM. Table \ref{table:performance} shows the execution times of Nami for the 8.8 Mw Maule scenario, obtained with an integrated and a dedicated graphics card (Intel HD Graphics 630 and NVIDIA Geforce GTX 1060 resp.) on the same computer. This is shown for a 3 minutes and 15 minutes resolution grid. Though the execution times  are very similar for the coarse grid, the simulation ran $5.16$ times faster with the dedicated GPU. This reveals on one side the increased efficiency when using a dedicated GPU, and on the other, that it is still possible to run the simulations with an integrated graphics card which can be more accessible.

\input{figures/performance.tex}


``Heat maps'' of maximum amplitudes and travel times isochrones are produced in python using the results of Nami.  Figures \ref{fig:energy2010} and \ref{fig:energy2011} show these results, where it is possible to see how the implemented numerical model represents tsunami propagation, including reflection and refraction patterns due to its interaction with bathymetry and shore lines.


Line plots showing time series of wave amplitude for each scenario are located in figures \ref{fig:timeseries2010} and \ref{fig:timeseries2011}. Each line corresponds respectively to:  Deep-ocean Assessment and Reporting of Tsunamis (DART) buoys measurementes after filtering out the astronomical tide using a low pass filter (black line); results of Nami (blue); and results of EasyWave (red), a software developed in the German Research Center for Geosciences, Potsdam \citep{christgau2014comparison} that implements the same numerical model explained in section \ref{sec:mathematical-model} with C++ and CUDA. DART buoys location and identification numbers are also shown with circles in figures \ref{fig:energy2010} and \ref{fig:energy2011}.

In both scenarios Nami and EasyWave show little differences between each other and an overall good agreement with measurements when examining the maximum amplitude, shape and arrival. A consistent delay is also observed with both models in the arrival of the first wave, which can grow from 0 to 15 minutes for the farthest points. As reported by \cite{abdolali2017role} and \cite{watada2014traveltime} this difference comes from neglecting effects such as water compressibility and earth's elasticity; the inclussion of these effects in the estimation of arrival times is considered as a future improvement of Nami. Overall these results show that Nami can produce results with the same level of accuracy as other similar tsunami simulation software, in good agreement with measurements.


\subsection{Example  2: TsunamiLab, a web platform of tsunami simulation}

The second use case of Nami illustrates how it can be integrated with other technologies and, at the same time, how this integration facilitates access to scientific knowledge to a new group of users, especially those who are not experts in the field. Specifically, we describe TsunamiLab \citep{tsunamilab}, a web platform for education and scientific outreach of tsunami hazard built using Nami, that aims at increasing the awareness and interest of people in science by providing interactive experiences.  TsunamiLab possesses several features that enable users to observe the propagation of tsunami waves around the world, and select and modify scenarios based on historical and “synthetic” data, i.e., user-defined earthquake location and magnitude.

Figure \ref{fig:screenshot-tsunamilab} shows the main view of TsunamiLab, available at \url{www.tsunamilab.cl}. TsunamiLab is built with React \citep{fedosejev2015react}, a Javascript library maintained by Facebook  that facilitates the development of complex interfaces under a reactive programming paradigm, structuring the application in terms of data flows and the state of individual components. The software architecture can then be described in a component tree as shown in figure \ref{fig:components-tsunamilab}. On this tree, components at the same level of depth can have access to the same state variables (such as the \verb;magnitude; and \verb;location; of the current scenario for the Nami, Canvas, Scene, Synth. Scenarios and Historical Scenarios components) and can pass them to the next component in the same branch. Components down the tree on a same branch can also receive callback functions to trigger changes in state variables of a parent component, triggering updates and data requests, for example.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{figures/tsunamilab.png}
    \caption{Screenshot of the main view of www.tsunamilab.cl }
    \label{fig:screenshot-tsunamilab}
\end{figure}

\begin{figure}
    \centering
    \input{figures/tsunamilab-component-tree.tex}
    \caption{Component tree describing the architecture of TsunamiLab}
    \label{fig:components-tsunamilab}
\end{figure}

TsunamiLab uses Nami for running the simulation and the Three.js library \citep{cabello2010three}  for displaying a 3D scene (in the Scene component of figure \ref{fig:components-tsunamilab}) where the Globe, Pins and other 3D graphics components are rendered. The integration of Nami with Three.js is performed by sharing  an HTML5 Canvas (the Canvas component of fig. \ref{fig:components-tsunamilab}) between them. This Canvas serves for the instatiation of the WebGL context used by Nami to run the simulations, and also for storing the ``heatmap'' with the colors of the simulation at every timestep, which is then displayed in the Globe of the Scene as a texture on top of the base map. 

The simulation domain covers the spherical rectangle $(lon, lat) \in [-70,70]$ $\times [-180,180]$ with a numerical grid of 10 minutes. The bathymetry is sampled from the ETOPO-1 dataset and compressed into a JPG image with the same resolution of the simulation to facilitate navigation. Quantization error from the JPG compression was found to be insignificant, since the only region where it becomes important is at the shores, which are excluded from the numerical domain of the simulator. The E-W boundary conditions are periodic whereas the N-S  open.

Historical scenarios are represented in the Globe as a collection of wave Pins scattered around the Globe at earthquake locations. Earthquake location, magnitude, and focal mechanism information are obtained through the publicly available USGS Earthquake Catalog web API; whenever the user clicks on a visible Pin, a request is sent to the USGS Earthquake Catalog and the shared \verb;magnitude; and \verb;location; states are updated, triggering the update of Nami with the new scenario. At the same time, text information is updated in the “Scenario Info” component (bottom right table in figure \ref{fig:screenshot-tsunamilab}) to give the user context about the current scenario.


Synthetic scenarios can be configured in a separate box in the top right corner of figure \ref{fig:screenshot-tsunamilab}. The magnitude can be chosen by increasing or decreasing the radius $r$ of the inner circle, on which case the magnitude of the new scneario is selected proportional to $r^3$. Earthquake location can also be selected by dragging and dropping the orange Pin on top of the globe. Nami is updated automatically whenever the \verb;location; or the \verb;magnitude; is changed through any of these two actions.

\subsubsection{Experience with non-expert users}

TsunamiLab has been presented on several ocassions where we have seen how these tools facilitate the communication of tsunami hazard to people of different ages. Examples of these participations are: the international competition ``Mathematics of Planet Earth 2017'' organized by Imaginary \citep{mpe17} and the Futur.E.S. festival organized by Cap Digital \citep{futures} (see picture (a) in figure \ref{fig:fotos}). Through these activities, we have noticed that it is possible to reach people and empower them to discover and formulate questions that can be valuable for a better understanding of tsunami risk. 

More specifically, with Nami we have extended the capabilities of TsunamiLab and improved the user interaction with interfaces that make it better suited for exhibitions. Examples of this are shown in pictures of figure \ref{fig:fotos}, where instead of a keyboard or a mouse we use a Sony PlayStation 3 Move Controller and a Leap Motion Controller as interfaces for user interactions. The former is a wireless joystick developed by Sony that, in addition to traditional buttons, includes a marker whose position is tracked by a camera and mapped accordingly into the scene to help with pointing actions such as rotating the Globe and changing the location of new scenarios; the latter consists of a set of infrared lights and camera that are used to infer the position of the user’s fingers, hands and arms,  enabling actions based on gestures. These interfaces have helped us explain complex phenomena such as tsunami wave propagation, refraction, reflection and diffraction to people of different ages in a more attractive and usable format, facilitating the communication of scientific results that are important for understanding tsunami hazard. 


\begin{figure}
%% https://tex.stackexchange.com/questions/140833/arranging-multiple-plots-in-a-grid-inside-a-figure-subfloat
    \centering
    \begin{tabular}{rc}
        (a) & \\
          & \includegraphics[width=65mm]{figures/foto3.jpg}\\
        (b) & \\
          & \includegraphics[width=40mm]{figures/foto4.jpg} \\
    \end{tabular}
    \caption{Pictures of activities performed with non-experts users. (a) Interactive display that uses the SONY \texttrademark PS Move Controller to configure earthquake magnitude and location, at the 2018 Paris FUTUR.E.S. technology festival organized by Cap Digital; (b) Interactive display that uses the Leap Motion Controller to configure earthquake magnitude and location using hand gestures at a local science fair in Chile.}
    \label{fig:fotos}
\end{figure}