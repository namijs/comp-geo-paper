\section{Architecture}
\label{sec:architecture}
\subsection{Overview}

Nami is designed to be integrated in web applications as shown in figure \ref{fig:architecture-context}. First, a user such as an emergency manager or any person interested on visualizing and configuring tsunami simulations, interacts with a Web Application through a Graphical User Interface, on which relevant information is displayed, possibly along with interaction controls such as buttons, menus, etc. Then this application uses Nami to simulate tsunamis given some desired configuration and data, which then turn into GPU instructions in the WebGL engine that runs the simulation efficiently. This basic structure reveals the necessity of configuring the simulation with custom data provided by the Application and the User; also that it is important to be able to change the simulation on the fly as the user interacts with the Application; and finally, that Nami should provide output that can conform to some standard, so results can be integrated into other systems.

\begin{figure}
    \centering
    \input{figures/architecture-overview.tex}
    \caption{Nami architecture overview for a standard use case}
    \label{fig:architecture-context}
\end{figure}


\subsection{Nami components details}

To explain how these three features are achieved in Nami, a “zoomed” architecture diagram is shown in figure \ref{fig:architecture-components}, detailing the components of Nami that facilitate the configuration, interactivity and integration of the simulations. First the Application should provide input data such as domain configuration, bathymetry and initial conditions. Some of this data may be in different formats such as CSV, JSON text files, or PNG and JPG images. This data is internally received by a Reader component, which contains methods to parse these different formats. The mission of the Reader is to convert this data into useful input for the Shallow Water Model component which can only receive input data in one format. Once the data is received, the Shallow Water Model component is the one in charge of executing the simulation one step at a time and call the WebGL instructions that use GPU acceleration to speed up the calculations. 

For running several iterations until a specific timestamp is reached, a Simulation Controller component is implemented, which not only runs the simulation program, but also calls Model functions every certain number of numerical iterations to fill a Canvas HTML5 element with a pseudo color map or colored texture, representing the values of a desired variable such as current wave heights, maximum heights or arrival times. Then, using the Controller, the Application can interrupt the simulation with common playback commands (play, pause, restart, etc) on user demand, and with the Model, the Application can collect the results of the simulation and populate its views. for example, by overlaying the canvas texture on a map or displaying a time series plot of a specific point of interest.


\begin{figure}
    \centering
    \input{figures/architecture-components.tex}
    \caption{Architecture of Nami in terms of its internal components}
    \label{fig:architecture-components}
\end{figure}

\subsection{Simulation life cycle}


Though the Shallow Water Model and Simulation Controller components help with interactivity and integration, it is important to notice that they are not enough. For example, one may be calling Model functions before the data is available, or even before the components exist;  or because it may be necessary to interrupt the simulation cycle under some specific condition, say, once the wave height is greater than a threshold or after a certain number of iterations. For this reason, we introduce the concept of “life cycle”, shown in figure \ref{fig:architecture-lifecycle}, describing the sequence of relevant stages since the data is received until the Controller has reached its final simulation time $t_f$. The application can then subscribe to events by providing a set of callback functions (listed on the left side of figure \ref{fig:architecture-lifecycle}), i.e., Application custom functions that are internally called at specific steps in the life cycle. 


\begin{figure}
    \centering
    \input{figures/architecture-lifecycle.tex}
    \caption{Life cycle diagram of a simulation in Nami.}
    \label{fig:architecture-lifecycle}
\end{figure}


\subsection{Model implementation in WebGL }


The Shallow Water Model component is the software representation of the algorithm and mathematical equations presented in section \ref{sec:mathematical-model}. It uses WebGL, an open web standard for GPU accelerated computer graphics on the web browser \citep{marrin2011webgl}, and as such, it requires one to adapt the implementation to concepts and design constraints of that field. The reason behind why it is useful for numerical computing is that every node in the numerical mesh depends only on a small number of neighbor nodes, making it analogous to image rasterization algorithms.

The rendering process is performed in a special type of program called Fragment Shader, written in a strong typed language called OpenGL Shading Language (GLSL), which assigns to each pixel in the target image its color through four values in the RGBA color space, i.e., three intensity levels for Red, Green and Blue and one for the transparency level (Alpha). Also, by design, a Shader program has no memory of previously rendered scenes so any prior information must be provided explicitly. For this reason, and for better performance, instead of rendering to the screen, Nami renders to a Frame Buffer Object (FBO), which is a WebGL object that allows storing the pixel data of a Shader into a texture that can be used as input later in other Shader instance. In total it uses two FBO’s: $FBO_1$ to store results of the previous time step and $FBO_2$ for storing new results that depend on $FBO_1$. On each pixel of a FBO, the RGBA values are stored corresponding to $(\eta, M, N, h)$ at each point in the simulation.

Figure \ref{fig:architecture-model} shows the flow of the simulation, internal to the Shallow Water Model component. After the data is received from the Reader component (top diagram), the bathymetry is rendered into a texture, the simulation timestep is calculated and the initial condition is rendered into $FBO_1$. Then, when the \verb;runSimulationStep; function of the Shallow Water Model component is called $FBO_1$ is assigned as the previous step texture and the simulation shader program is run, with its output stored into $FBO_2$. The flow ends with $FBO_1$ and $FBO_2$ being swapped, so the data is ready for when the \verb;runSimulationStep; function is called again.



% \begin{figure}
%     \centering
%     \input{figures/architecture-model.tex}

% \end{figure}



\begin{figure}
    \centering
    \input{figures/fbo_iterations.tex}
    \caption{Diagram depicting how the timestepping scheme works with the WebGL rendering pipeline.}
    \label{fig:architecture-model}
\end{figure}