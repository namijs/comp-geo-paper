REV=$(git rev-parse dev)
REV_NEW=$(git rev-parse HEAD)
echo "Creating diff w.r.t HEAD of dev branch: "$REV 
# NAME_ROOT=main.bundle-diff$REV

# latexdiff-vc --git  -r $REV --pdf --exclude-safecmd=citep --force main.bundle.tex
git latexdiff -o diff.pdf  --exclude-safecmd=cite --exclude-safecmd=citep --bbl --main main.bundle.tex $REV --
open diff.pdf